var gulp = require('gulp');
var rename = require('gulp-rename');
var build = require('gulp-build');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");


/* 初始化开发目录
 * vendor : bower 安装目录
 * css 	: css开发目录
 * js	：js开发目录
 * less : less开发目录
 * sass : sass开发目录
 */
gulp.task('init', function(){
	gulp.src(' ')
	.pipe(gulp.dest('app/vendor'))
	.pipe(gulp.dest('app/css'))
	.pipe(gulp.dest('app/js'))
	.pipe(gulp.dest('app/less'))
	.pipe(gulp.dest('app/sass'));

	console.log('开发目录初始化完成...')
})

/*
 * 复制 html 文档
 */
gulp.task('html', function(){
 	gulp.src(['app/**/*.html','app/**/*.htm'])
 	.pipe(gulp.dest('dist/'));
 })

 /*
  * 合并压缩项目 js 
  *
  */
gulp.task('script', function(){
	gulp.src([
		'app/js/app.js'
		// 继续按依赖关系的顺序添加 js 库，如果无需依赖，直接写为： app/js/**/*.js
		])
		.pipe(concat('app.js'))
		.pipe(gulp.dest('dist/js/'))
		.pipe(uglify())
		.pipe(rename('app.min.js'))
		.pipe(gulp.dest('dist/js/'));
  })

/*
 * 合并压缩 js 库
 */
gulp.task('script-lib', function(){
	gulp.src([
		'app/vendor/jquery/dist/jquery.js',
		'app/vendor/bootstrap/dist/js/bootstrap.js'
		// 继续按依赖关系的顺序添加 js 库
		])
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest('dist/js/'))
		.pipe(uglify())
		.pipe(rename('vendor.min.js'))
		.pipe(gulp.dest('dist/js/'));
 })

 /*
  * 合并压缩 css
  */
gulp.task('style', function(){
 	gulp.src([
 		'app/vendor/bootstrap/dist/css/bootstrap.css',
 		'app/css/style.css'
 		])
 		.pipe(concat('style.css'))
 		.pipe(gulp.dest('dist/css/'))
 		.pipe(minifyCss())
 		.pipe(gulp.dest('dist/css/'));
 })

/*
 * sass 编译
 */
gulp.task('sass', function(){
	return gulp.src(['app/sass/**/*.scss'])
		//.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(sass())
		.pipe(rename('sass.css'))
		.pipe(gulp.dest('dist/css/'))
		.pipe(minifyCss('sass.min.css'))
		.pipe(gulp.dest('dist/css/'))
	
 })

 /*
  * less 编译
  */
gulp.task('less', function(){
	gulp.src('app/less/**/*.less')
		.pipe(less())
		.pipe(rename('less.css'))
		.pipe(gulp.dest('dist/css/'))
		.pipe(minifyCss('less.min.css'))
		.pipe(gulp.dest('dist/css/'))
 })

/*
 * 极限压缩 css 与 js
 */
gulp.task('limit', function(){
	gulp.src([
		'dist/css/style.css',
		'dist/css/sass.css',
		'dist/css/less.css'
		])
		.pipe()
})

 /*
  * 开发专用服务器
  */
gulp.task('server', function(){
	return gulp.src('./')
		.pipe(server({
			host: '127.0.0.1',
			port: 8088,
			livereload: true,
			// directoryListing: true,
			defaultFile: 'index.html',
			open: true
		}));
 })

 /*
  * 编译后测试使用服务器
  */
gulp.task('server-build', function(){
	gulp.src('./dist')
		.pipe(server({
			host: 'localhost',
			port: 8000,
			livereload: false,
			// directoryListing: true,
			defaultFile: 'index.html',
			open: true
		}))
 })

/*
 * 监听文件
 */
gulp.task('watch', function(){
	gulp.watch(['app/sass/**/*.scss','app/sass/**/*.sass'],['sass']);
	gulp.watch(['app/sass/**/*.less'],['less']);
})

/*
 * 编译项目
 */
gulp.task('build',['init','html','script-lib','script','style','sass','less','limit'], function(){
	console.log('编译完成...');
 })

 /*
  * 开发项目
  */
gulp.task('dev', ['init'], function(){

 })

/*
 * 默认
 */
gulp.task('default',['server','watch'],function(){
	console.log('服务器已启动，尽情开发吧')
})

/*
 * 开发博客
 */
 gulp.task('blog',function(){
 	return gulp.src('./Blog/')
		.pipe(server({
			host: '127.0.0.1',
			port: 8088,
			livereload: true,
			// directoryListing: true,
			defaultFile: 'index.html',
			open: true
		}));
 })