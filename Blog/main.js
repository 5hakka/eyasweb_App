window.info = {};
window.info.base_url = window.location.origin;
window.info.tplRootPath = window.location.origin+'/app/tpl/';
requirejs.config({
	base_url:'/',
	paths:{
		// 模块
		// modules:'app/modules',
		// boot:'app/modules/Boot',
		// model:'app/modules/Model',
		// collection:'app/modules/Collection',
		// router:'app/modules/Router',
		// view:'app/modules/View',

		// angular module
		controller:'app/modules/Controller',
		directive:'app/modules/Directive',
		filter:'app/modules/Filter',
		router:'app/modules/Router',
		service:'app/modules/Service',
		boot:'app/modules/Boot',


		// 实用工具
		util:'app/utils',

		// 库
		libs:'libs',
		backbone:'libs/backbone/backbone',
		underscore:'libs/underscore/underscore',
		jquery:'libs/jquery/dist/jquery',
		bootstrap:'libs/bootstrap/dist/js/bootstrap',
		angular:'libs/angular/angular',
		"ui-router":'libs/angular-ui-router/release/angular-ui-router',
		"ui-bootstrap":'libs/angular-ui-bootstrap',

	},
	shim:{
		jquery: {
			exports: "$"
		},
		underscore: {
			exports: "_"
		},
		backbone:{
			deps:['jquery','underscore'],
			exports:'Backbone'
		},
		bootstrap:{
			deps:['jquery'],
			exports: "$.fn.alert"
		},
		angular:{
			exports:'angular'
		},
		"ui-router":{
			deps:['angular']
		},
		"ui-bootstrap":{
			deps:['angular']
		}
	}

})
define(['boot/main']);