define(['jquery','angular','service/main'],function($,angular){
	var controller = angular.module('postController',['post']);
	controller.controller('listController',function($scope,Post){
		Post.getList().success(function(data){
			$scope.posts = data;
		}).error(function(e){
			console.error(e);
		})
	});
	return controller;
})