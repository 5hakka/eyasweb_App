define(['jquery','angular','controller/header','controller/post','controller/footer','controller/global'],function($,angular){
	var controller = angular.module('controller',['headerController','postController','footerController','globalController']);

	return controller;
})