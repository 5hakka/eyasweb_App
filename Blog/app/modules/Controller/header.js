define(['jquery','angular','service/main'],function($,angular){
	var controller = angular.module('headerController',['service']);
	controller
	.controller('TopController',function($scope,header){
		$scope.Left = {};
		$scope.Right = {};
		header.getTopLeft().success(function(data){
			$scope.Left.NavItems = data;
		});
		header.getTopRight().success(function(data){
			$scope.Right.NavItems = data;
		})
	})
	.controller('LogoController',function($scope,header){
		$scope.Logo = header.getLogo();
	})
	.controller('NavbarController',function($scope,header){
		// $scope.
		header.getNavbar().success(function(data){
			$scope.NavItems = data;
		})
	})


	
	return controller;
})