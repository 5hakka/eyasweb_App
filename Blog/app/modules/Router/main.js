define(['jquery','angular','ui-router','controller/main'],function($,angular){
	var router = angular.module('router',['ui.router','controller']);
	router.config(function($stateProvider,$urlRouterProvider){
	  $urlRouterProvider.otherwise('/post');
		$stateProvider
		.state('post',{
			url:'/post',
			abstract: true,
			views:{
				'':{
					templateUrl:'app/tpl/layout/layout.html'
				},
				'top@post':{
					templateUrl:'app/tpl/header/top.html',
					controller:'TopController'
				},
				'logo@post':{
					templateUrl:'app/tpl/header/logo.html',
					controller:'LogoController'
				},
				'navbar@post':{
					templateUrl:'app/tpl/header/navbar.html',
					controller:'NavbarController'
				},
				'sidebar@post':{
					templateUrl:'app/tpl/main/sidebar.html',
					// controller:'SidebarController'
				},
				'footer@post':{
					templateUrl:'app/tpl/footer/footer.html',
					// controller:'FooterController'
				}
			}
		})
		.state('post.list',{
			url:'',
			views:{
				'main':{
					templateUrl:'app/tpl/main/list.html',
					controller:'listController'
				}
			}
		})
		.state('post.detail',{
			url:'/:nid',
			views:{
				'main':{
					templateUrl:'app/tpl/main/detail.html'
				}
			}
		})
	});
	return router;
});