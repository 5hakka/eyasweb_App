define(['jquery','angular'],function($,angular){
	var global = angular.module('global',['header']);
	global.constant('global',{
		base_url:'http://eyasweb.com/',
		base_path:'Blog/',
		base_api:'http://api.eyaslife.com/',
		analog_path:'app/data/',
	})
	return global;
})