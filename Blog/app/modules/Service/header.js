define(['jquery','angular','service/global'],function($,angular){
	var header = angular.module('header',['global']);
	header.factory('header',function($http,global){
		var _getTopLeft = function(){
			return promise = $http({
				method:'GET',
				url:global.analog_path+'header/topLeft.json',
				dataType:'json'
			});
		}
		var _getTopRight = function(){
			return promise = $http({
				method:'GET',
				url:global.analog_path+'header/topRight.json',
				dataType:'json'
			});
		}
		var _getLogo = function(){
			return {
				imgUrl:'http://www.bootcss.com/p/flat-ui/images/illustrations/retina.png'
			}
		}
		var _getNavbar = function(){
			return promise = $http({
				method:'GET',
				url:global.analog_path+'header/navbar.json',
				dataType:'json'
			});
		}
		return {
			getTopLeft: _getTopLeft,
			getTopRight: _getTopRight,
			getLogo: _getLogo,
			getNavbar: _getNavbar
		}
	})
	return header;
})