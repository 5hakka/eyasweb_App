define(['jquery','angular','service/global'],function($,angular){
	var post = angular.module('post',['global']);
	post.factory('Post',function($http,global){
		var _getList = function(){
			return $http({
				method:'GET',
				url:global.base_api+'blog-list'
			});
		};
		var _getNode = function(nid){
			if(!nid) return;
			return $http({
				method:'GET',
				url:global.base_api+'blog_api/node'+nid
			});
		};
		return {
			getList:_getList,
			getNode:_getNode
		}
	})
	return post;
})