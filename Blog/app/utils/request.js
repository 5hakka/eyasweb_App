define(["jquery"],
function($) {
	$.r=r;
	function r(url,data,type,datatype,success,error,async){
		if(!url) return;
		if(!data)data={};
		if(!type)type='POST';
		else type=type.toUpperCase();
		if(!datatype) datatype='json';
		if(typeof async=='undefined') async=true;
		else async=!!async;
		if(!success) success=function(){};
		if(!error) error=function(e){
			e.url=url;
			e.data=data;
			console.log(e);
		};
		
		return $.ajax({
			url:url,
			data:data,
			type:type,
			async:async,
			dataType:datatype,
			success:function(data){
					success(data);
			},
			error:function(data){
				error(data);
			},
		});
	}
	return r;
	
});







