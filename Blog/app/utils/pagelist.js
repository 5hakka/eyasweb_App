define(['jquery','underscore','util/request'],function($,_){
	return {
		init:function(){},
		loadTpl:function(url,data,success,error,callback){
			var respon = $.ajax({
				url:url,
				method:'GET',
				async:false,
				dataType:'text',
				complete:callback
			});
			if(data){
				var tpl = _.template(respon.responseText);
				var html = tpl(data);
				return html;
			}else{
				return respon.responseText
			}
		}
	}
})